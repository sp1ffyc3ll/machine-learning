#!/usr/bin/python

import optparse
import pandas as pd
import numpy as np

verbose = False


def mse(slope, intercept, x_train, y_train):
    rss = 0
    prediction = []

    for i in range(len(x_train)):
        # add our prediction
        prediction.append((slope * x_train[i]) + intercept)
        # get the residue, square it so that it's always a positive value
        rss += (y_train[i] - ((slope * x_train[i]) + intercept))**2
    # calculate the mean value of residual sum of squares
    error = rss / len(x_train)
    return error, prediction


def gradientDescent(slope, intercept, learningRate, numIterations, x_train, y_train):
    for i in range(numIterations):
        # set initial conditions
        partialDerivSlope = 0
        partialDerivIntercept = 0
        dataSize = float(len(x_train))

        for j in range(len(x_train)):
            # calculate the partial derivatives with respect to slope and 
            partialDerivSlope += (-(2/dataSize) * x_train[j] * (y_train[j] - (slope * x_train[j]) + intercept))
            partialDerivIntercept += (- (2/dataSize) * (y_train[j] - (slope * x_train[j]) + intercept))

        slope = slope - (learningRate * partialDerivSlope)
        intercept = intercept - (learningRate * partialDerivIntercept)

    return slope, intercept


def linearModel(train, test):
    global verbose
    # divy up x,y training data
    x_train = []
    y_train = []

    for i in range(len(train)):
        x_train.append(train[i][0])
        y_train.append(train[i][1])

    # set initial conditions
    slope = 0
    intercept = 0
    # set gradient descent params
    numIterations = 1000
    learningRate = 0.0001

    # get MSE of initial conditions
    error, prediction = mse(slope, intercept, x_train, y_train)

    if verbose:
        print('Initial error: {0}'.format(error))
        print('Optimizing...')

    # optimize slope and intercept values via gradient descent
    gradientSlope, gradientIntercept = gradientDescent(slope, intercept, learningRate, numIterations, x_train, y_train)
    # get MSE of optimized conditions
    error, prediction = mse(gradientSlope, gradientIntercept, x_train, y_train)

    if verbose:
        print('Final error: {0}'.format(error))

    # calculate response based on test value
    response = int((gradientSlope * test) + gradientIntercept)
    return response


def main():
    global verbose
    # define options
    usage = "usage: %prog --train <dataFile>.csv"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option('--train', dest='trainData', help="data to train the linear model on")
    parser.add_option('--test', dest='testDataPoint', help="dataPoint you'd like a prediction for")
    parser.add_option('-v', '--verbose', action='store_true', dest='verbose', help="enable more verbose output")
    #parser.add_option('-r', '--regression', action='store_true', dest='regression', help="enable if your task involves regression")
    #parser.add_option('-c', '--classify', action='store_false', dest='regression', help="enable if your task involves classification")
    (options, args) = parser.parse_args()

    # create initial datasets
    train = pd.read_csv(options.trainData).values.tolist()
    # grab test value
    if options.testDataPoint:
        test = int(options.testDataPoint)
    else:
        test = int(input('Which value would you like to predict for? '))

    if options.verbose == True:
        verbose = True

    # run the algorithm
    prediction = linearModel(train, test)
    # output the result
    print("Predicted response of input {0}: {1}".format(test, prediction))


if __name__ == "__main__":
    main()