#!/usr/bin/python

from collections import Counter
import math
import optparse
import pandas as pd
import sys


def euclidean_distance(x, y):
    distance = 0
    for i in range(len(x)):
        distance += (x[i] - y[i])**2
    
    return math.sqrt(distance)


def mean(labels):
    return sum(labels)/len(labels)


def meanWithBoundary(labels):
    prediction = 0
    mean = sum(labels)/len(labels)

    # boundary condition
    if mean >= 0.5:
        prediction = 1
    else:
        pass
    return prediction


def knn(data, query, k, choice):
    distances_indices = []

    # get distance between each datapoint and our querypoint
    for index, dataPoint in enumerate(data):
        distance = euclidean_distance(dataPoint[:-1], query)
        distances_indices.append((distance, index))

    # sort the distances
    distances_indices = sorted(distances_indices)
    # get the first k distances
    distances_indices = distances_indices[:k]
    # get the node labels associated with k nearest distances
    knn_labels = [data[i][1] for distance, i in distances_indices]

    # perform choice function on the labels
    return distances_indices, choice(knn_labels)


def main():
    # define options
    usage = "usage: %prog [--regression|--classify] -f <file>.csv"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option('-f', '--filename', dest='filename', help="name of the CSV file you'd like analyzed")
    parser.add_option('-r', '--regression', action='store_true', dest='regression', help="enable if your task involves regression")
    parser.add_option('-c', '--classify', action='store_false', dest='regression', help="enable if your task involves classification")
    (options, args) = parser.parse_args()

    # choice function depends on task type
    if options.regression == True:
        choice_fn = mean
    else:
        choice_fn = meanWithBoundary

    # grab dataset and the value we'd like an outcome associated with
    data = pd.read_csv(options.filename).values.tolist()
    queryValue = [float(input("Which value would you like to query? "))]
    queryString = input("What are you trying to determine? ")
    k = 5

    neighbors, prediction = knn(data, queryValue, k, choice=choice_fn)

    # ONLY translate if task is classification
    if options.regression == False:
        # binary to human-readable
        if prediction == 1:
            prediction = "yes"
        else:
            prediction = "no"

    # output results
    print("\nk-Nearest Neighbors: {0}".format(neighbors))
    print("Prediction ({0})?: {1}".format(queryString, prediction))


if __name__ == "__main__":
    main()